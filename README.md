# Sequenc Alignment 
This is a Sequence Alignment algorithm that uses dynamic programming to determine how similar two strings are.  
The main purpose of this is in bioinformatics where we want to determine how similar two pieces of genetic code are.