import random


def sequence_alignment(input_x, input_y, p_xy, p_gap, p_gucci):
    m = len(input_x)
    n = len(input_y)

    dp = [[0 for x in range(m + n + 1)] for y in range(m + n + 1)]

    # set edges of array to increasing gap
    for Z in range(0, m + n + 1):
        dp[Z][0] = Z * p_gap
        dp[0][Z] = Z * p_gap

    # print(dp[0][6])

    # compute each step
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if input_x[i - 1] == input_y[j - 1]:
                dp[i][j] = dp[i - 1][j - 1] + p_gucci
            else:
                dp[i][j] = max(dp[i - 1][j - 1] + p_xy, dp[i - 1][j] + p_gap, dp[i][j - 1] + p_gap)

    print("Alignment Score: " + str(dp[m][n]))

    length = m + n

    x_prime = ["_" for x in range(length)]
    y_prime = ["_" for x in range(length)]

    y_prime_i = length - 1
    x_prime_i = length - 1

    i = m
    j = n

    while i > 0 and j > 0:
        if input_x[i - 1] == input_y[j - 1]:
            x_prime[x_prime_i] = input_x[i - 1]
            y_prime[y_prime_i] = input_y[j - 1]
            y_prime_i = y_prime_i - 1
            x_prime_i = x_prime_i - 1
            i = i - 1
            j = j - 1
        elif (dp[i - 1][j - 1] + p_xy) == dp[i][j]:
            x_prime[x_prime_i] = input_x[i - 1]
            y_prime[y_prime_i] = input_y[j - 1]
            y_prime_i = y_prime_i - 1
            x_prime_i = x_prime_i - 1
            i = i - 1
            j = j - 1
        elif dp[i - 1][j] + p_gap == dp[i][j]:
            x_prime[x_prime_i] = input_x[i - 1]
            y_prime[y_prime_i] = '_'
            y_prime_i = y_prime_i - 1
            x_prime_i = x_prime_i - 1
            i = i - 1
        elif dp[i][j - 1] + p_gap == dp[i][j]:
            x_prime[x_prime_i] = '_'
            y_prime[y_prime_i] = input_y[j - 1]
            y_prime_i = y_prime_i - 1
            x_prime_i = x_prime_i - 1
            j = j - 1

    while x_prime_i > 0:
        if i > 0:
            x_prime[x_prime_i] = input_x[i - 1]
        else:
            x_prime[x_prime_i] = '_'
        i = i - 1
        x_prime_i = x_prime_i - 1

    while y_prime_i > 0:
        if j > 0:
            x_prime[y_prime_i] = input_y[j - 1]
        else:
            y_prime[y_prime_i] = '_'
        j = j - 1
        y_prime_i = y_prime_i - 1

    stop = 0

    for w in range(length - 1, 0, -1):
        if y_prime[w] == '_' and x_prime[w] == '_':
            stop = w
            break

    x_prime = x_prime[stop + 1:]
    y_prime = y_prime[stop + 1:]

    print("aligned x: " + str(''.join(x_prime)))
    print("aligned y: " + str(''.join(y_prime)))


def random_base():
    r = random.randint(0, 3)
    if r == 0:
        return 'A'
    elif r == 1:
        return 'T'
    elif r == 2:
        return 'G'
    elif r == 3:
        return 'C'


def generate_gene(length):
    s = ""

    for i in range(length):
        s = s + random_base()

    return s


def main():

    gene_x = generate_gene(1000)
    gene_y = generate_gene(1020)

    print(gene_x)
    print(gene_y)
    print()

    sequence_alignment(gene_x, gene_y, -1, -2, 1)


if __name__ == "__main__":
    main()
